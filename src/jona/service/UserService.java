package jona.service;

import static jona.utils.CloseableUtil.*;
import static jona.utils.DBUtil.*;

import java.sql.Connection;

import jona.beans.User;
import jona.dao.UserDao;
import jona.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void edit(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);

				UserDao userDao = new UserDao();
				userDao.edit(connection, user);

				commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public User getEditUser(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

			User user = userDao.getEditUser(connection, id);

			commit(connection);

			return user;
		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void noPassEdit(User user) {
		Connection connection = null;
		try {
			connection = getConnection();


				UserDao userDao = new UserDao();
				userDao.noPassEdit(connection, user);

				commit(connection);

		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}



}
