package jona.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jona.beans.Comment;
import jona.beans.User;
import jona.beans.UserMessage;
import jona.service.CommentService;
import jona.service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{



		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		boolean isShowCommentForm;

		if(user != null) {
			isShowMessageForm = true;
		}else {
			isShowMessageForm = false;
		}

		if(isShowMessageForm != false) {
			isShowCommentForm = true;
		}else {
			isShowCommentForm = false;
		}

		String START_DATE = (String) request.getParameter("start_date");

		String END_DATE = (String) request.getParameter("end_date");

		request.setCharacterEncoding("UTF-8");

		String CATEGORY = (String) request.getParameter("category");

		System.out.println(CATEGORY);


		List<UserMessage> messages = new MessageService().getMessage(START_DATE, END_DATE, CATEGORY);
		List<Comment> comments = new CommentService().getComment();


		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("isShowCommentForm", isShowCommentForm);
		request.setAttribute("start_date", START_DATE);
		request.setAttribute("end_date", END_DATE);
		request.setAttribute("category", CATEGORY);

		request.getRequestDispatcher("/home.jsp").forward(request, response);


	}


}












