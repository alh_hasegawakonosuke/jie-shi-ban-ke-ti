<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored ="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>
		<title>ログイン</title>

	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session"/>
	            </c:if>



			<div class="login-form">
			<div class="type-shine">掲示板。</div>
				<form action="login" method="post"><br />
					<ul>
						<li class="login_id">
			                <label for="login_id">ログインID</label>
			                <input name="login_id" id="login_id" value="${login_id}" /> <br />
						</li>
						<li class="password">
			                <label for="password">パスワード</label>
			                <input name="password" type="password" id="password"/> <br />
						</li>

						<li><input class="button" type="submit" value="ログイン" /> <br />
	            		</li>
            		</ul>
	            </form>
            </div>

            <div class="copyright"> Copyright(c)Jonathan</div>
        </div>



    </body>
</html>