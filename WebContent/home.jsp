<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<meta http-equiv="Cotent-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>

		<script type="text/javascript">
	<!--

	function check(){

		if(window.confirm("投稿を削除してよろしいですか")){
			return ture;
		}else{
			window.alert("キャンセルしました");
			return false;
		}
	}

	function check2(){

		if(window.confirm("コメントを削除してよろしいですか")){
			return ture;
		}else{
			window.alert("キャンセルしました");
			return false;
		}
	}
	function ShowLength( str ) {
		   document.getElementById("inputlength").innerHTML = str.length + "文字";
		}



	// -->
	</script>

	</head>
	<body>
	<div class="main-contents">


		<div class="header">

			<a href="newMessage" class="btn-border">新規投稿画面</a>
			<c:if test="${loginUser.department_id == 1}">
			<a href="userManagement" class="btn-border">ユーザー管理画面</a>
			</c:if>
			<a href="logout" class="btn-border">ログアウト</a>
		</div>

		<div class="errorMessages">
			<c:if test="${not empty errorMessages}">
				<span class="errorMessages">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}"/>
					</c:forEach>
				</span>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
		</div>


			<div class="search">

				<form action="./" method="get" >



							<input class="start" type="date" name="start_date" value="${start_date}"/>～



							<input class="end" type="date" name="end_date" value="${end_date }"/><br />


							<input class="category" name="category" id="category" placeholder="カテゴリー" value="${category}"/>

						<input class="button" type="submit" value="絞り込み" class="search-button"/>

				</form>

			</div>





		<div class="message-form">
			<c:forEach items="${messages}" var="message">
			<div class="post_message">
				<div class="message">

						<div class="subject-box">
							<span class="subject-title">件名</span>
							<p class="subject"><c:out value="${message.subject}"/></p>
						</div>

						<div class="text-box">
							<span class="text-title">本文</span>
							<p class="text"><c:forEach var="text" items="${fn:split(message.text, '
										')}">
								<div>${text}</div>
							</c:forEach></p>
						</div>

						<div class="category-box">
							<span class="category-title">カテゴリー</span>
							<p class="category"><c:out value="${message.category}"/></p>
						</div>

						<input type="hidden" name="user_id" value="${message.user_id }"/>

						<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<div class="name"><c:out value="${message.name}"/>さんの投稿</div>

						<c:if test="${loginUser.id == message.user_id}">
							<form action="msdelete" method="post" onSubmit="return check()" ><br/>
								<input type="hidden" name="message_id" value="${message.id}"/>
								<input class="button" type="submit" value="投稿削除">
							</form>
						</c:if>

				</div>


					<div class="comments">
						<p><c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.message_id}">
								<span class="comment">
									<c:forEach var="s" items="${fn:split(comment.text, '
										')}">
										<c:out value="${s}"></c:out><br/>
									</c:forEach>
									<span class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>

								</span><br />

									<input type="hidden" name="commnet.user_id" value="${comment.user_id}"/>
									<span class="comment-dalete">
										<c:if test="${loginUser.id == comment.user_id}">

											<form action="delete" method="post" onSubmit="return check2()">
												<input type="hidden" name="user.id" value="${user.id}"/>
												<input type="hidden" name="comment.id" value="${comment.id}"/>
												<input class="button" type="submit" value="コメント削除"/><br/>
											</form>
										</c:if>
									</span>
							</c:if>
						</c:forEach></p>

						<form action="comment" method="post"><br />
							<label for="comment">コメント投稿</label>
							<textarea name="comment" cols="50" rows="3" wrap="hard" placeholder="ここにコメントを入力してください" onkeyup="ShowLength(value);" /></textarea>
							<p id="inputlength">0文字</p>
							<input type="hidden" name="message_id" value="${message.id }" />
							<input class="button" type="submit" value="投稿"/>
						</form>
					</div>
			</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)Jonathan</div>
		</div>
	</body>
</html>